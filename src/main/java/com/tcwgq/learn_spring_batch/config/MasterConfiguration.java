package com.tcwgq.learn_spring_batch.config;

import com.tcwgq.learn_spring_batch.batch.IdRangePartitioner;
import com.tcwgq.learn_spring_batch.batch.ItemJobExecutionListener;
import com.tcwgq.learn_spring_batch.support.BatchIncrementer;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.partition.PartitionHandler;
import org.springframework.batch.integration.partition.MessageChannelPartitionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.core.MessagingTemplate;

import javax.sql.DataSource;

@Configuration
public class MasterConfiguration {
    private static final int GRID_SIZE = 10;

    @Autowired
    private JobExplorer jobExplorer;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean("masterJob")
    public Job job(@Qualifier("masterStep") Step masterStep) {
        return jobBuilderFactory.get("masterJob")
                .start(masterStep)
                .incrementer(new BatchIncrementer())
                .listener(new ItemJobExecutionListener())
                .build();
    }

    @Bean("masterStep")
    public Step masterStep(@Qualifier("slaveStep") Step slaveStep, DataSource dataSource, MessagingTemplate messagingTemplate) throws Exception {
        return stepBuilderFactory.get("masterStep")
                .partitioner(slaveStep.getName(), new IdRangePartitioner(dataSource))
                .partitionHandler(partitionHandler(messagingTemplate))
                .step(slaveStep)
                .build();
    }

    @Bean
    public PartitionHandler partitionHandler(MessagingTemplate messagingTemplate) throws Exception {
        MessageChannelPartitionHandler partitionHandler = new MessageChannelPartitionHandler();
        partitionHandler.setStepName("slaveStep");
        partitionHandler.setGridSize(GRID_SIZE);
        partitionHandler.setMessagingOperations(messagingTemplate);
        partitionHandler.setPollInterval(5000L);
        partitionHandler.setJobExplorer(jobExplorer);
        partitionHandler.afterPropertiesSet();
        return partitionHandler;
    }
}
